# from django.shortcuts import render
from django.contrib.auth.models import User
import django.http
from rest_framework import status, viewsets, permissions
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Recipient, Suggestion
from api.serializers import UserSerialzer, \
    RecipientSerializer, SuggestionSerializer


class UserCreateView(CreateAPIView):
    model = User
    permission_classes = [
        permissions.AllowAny    # Or anon users can't register
    ]
    serializer_class = UserSerialzer


class RecipientViewSet(viewsets.ModelViewSet):
    queryset = Recipient.objects.all()
    serializer_class = RecipientSerializer


class SuggestionViewSet(viewsets.ModelViewSet):
    queryset = Suggestion.objects.all()
    serializer_class = SuggestionSerializer


class Recipients(APIView):
    def get(self, request, format=None):
        recipients = list(Recipient.objects.all())
        serializer = RecipientSerializer(recipients, many=True)
        return Response(serializer.data)


# class Suggestions(APIView):
#     def get(self, request, format=None):
#         suggestions = list(Suggestion.objects.all())
#         return Response(suggestions)


class RecipientDetail(APIView):
    def get_object(self, pk):
        try:
            return Recipient.objects.get(pk=pk)
        except Recipient.DoesNotExist:
            raise django.http.Http404

    def get(self, request, pk, format=None):
        recipient = self.get_object(pk)
        serializer = RecipientSerializer(recipient)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = RecipientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        recipient = self.get_object(pk)
        serializer = RecipientSerializer(recipient, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        recipient = self.get_object(pk)
        recipient.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SuggestionDetail(APIView):
    def get_object(self, pk):
        try:
            return Suggestion.objects.get(pk=pk)
        except Suggestion.DoesNotExist:
            raise django.http.Http404

    def get(self, request, pk, format=None):
        suggestion = self.get_object(pk)
        serializer = SuggestionSerializer(suggestion)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SuggestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        suggestion = self.get_object(pk)
        serializer = SuggestionSerializer(suggestion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        suggestion = self.get_object(pk)
        suggestion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
