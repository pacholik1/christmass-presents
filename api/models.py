from django.db import models
from django.contrib.postgres.fields import ArrayField


class Recipient(models.Model):
    recipientname = models.CharField(max_length=255)
    # suggestions = Suggestion[]
    # suggestions = models.ManyToManyField('Suggestion', through='recipient')


class Suggestion(models.Model):
    class Status(models.TextChoices):
        considered = "considered"
        ordered = "ordered"
        paid = "paid"
        delivered = "delivered"

    label = models.CharField(max_length=255)
    urls = ArrayField(
        models.CharField(max_length=255),
        default=list,
        blank=True,
    )
    status = models.CharField(choices=Status.choices,
                              default=Status.considered,
                              max_length=255)
    recipient = models.ForeignKey(Recipient,
                                  on_delete=models.CASCADE,
                                  related_name='suggestions')
