from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Recipient, Suggestion


class UserSerialzer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name',
                  'password']

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        print(validated_data)

        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
        )
        return user


class SuggestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Suggestion
        fields = ['id', 'label', 'urls', 'status', 'recipient']


class RecipientSerializer(serializers.ModelSerializer):
    suggestions = SuggestionSerializer(many=True, read_only=True)

    class Meta:
        model = Recipient
        fields = ['id', 'recipientname', 'suggestions']
