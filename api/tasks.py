from celery import shared_task
import notify2


notify2.init('api')


@shared_task
def notification():
    print('notification')
    n = notify2.Notification('summary', 'message')
    n.show()
