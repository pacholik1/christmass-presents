"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
import rest_framework_simplejwt.views as jwtviews

# from . import views
import api.views


router = routers.DefaultRouter()
router.register(r'recipients', api.views.RecipientViewSet)
router.register(r'suggestions', api.views.SuggestionViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),

    path('token/', jwtviews.token_obtain_pair),
    path('token/refresh/', jwtviews.token_refresh),
    path('signup/', api.views.UserCreateView.as_view()),

    path('', include(router.urls)),
    path('recipient/<int:pk>', api.views.RecipientDetail.as_view()),
    # path('recipients/', api.views.Recipients.as_view()),
    path('suggestion/<int:pk>', api.views.SuggestionDetail.as_view()),
]
