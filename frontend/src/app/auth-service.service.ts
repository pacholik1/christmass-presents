import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {map, shareReplay} from 'rxjs';
import jwt_decode from 'jwt-decode';
import {User} from './model';

interface AuthResult {
  access: string,
  refresh: string,
}

interface Token {
  token_type: string,
  exp: number,
  iat: number,
  jti: string,
  user_id: number,
}

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(
    private http: HttpClient,
  ) { }

  login(username: string, password: string) {
    // return this.http.post<{username: string, password: string}>(
    return this.http.post<AuthResult>(
      'http://localhost:8000/token/',
      {username, password}
    ).pipe(
      map(result => this.setSession(result)),
      shareReplay()
    );
  }

  decodeToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

  private setSession(authResult: AuthResult) {
    // const expiresAt = moment().add(authResult.expiresIn,'second');

    localStorage.setItem('token', authResult.access);
    // localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  }          

  public isLogged(): boolean {
    const token = localStorage.getItem('token');
    if (!token) return false;

    const decoded: Token = this.decodeToken(token);
    let d: Date = new Date();
    // console.log( d.getTime(), decoded.exp*1000 );
    // console.log( d.getTime() < decoded.exp*1000 );
    return d.getTime() < decoded.exp*1000;
  }

  signup(username: string, email: string, password: string) {
    return this.http.post<User>(
      'http://localhost:8000/signup/',
      {username, email, password}
    ).pipe(
    shareReplay()
    );
  }
}
