import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { PresentsService } from '../presents.service'
import { Recipient, Status, Suggestion } from '../model'
import {lastValueFrom} from 'rxjs';

@Component({
  selector: 'app-present-list',
  templateUrl: './present-list.component.html',
  styleUrls: ['./present-list.component.scss']
})
export class PresentListComponent implements OnInit {
  status!: Status;
  statuskeys: string[] = [];
  // recipients = this.presentsService.getRecipients();
  recipients!: Recipient[];
  suggestions!: Suggestion[];

  constructor(
    private presentsService: PresentsService,
    private dialog: MatDialog,
  ) {
    this.statuskeys = Object.keys(Status);
  }

  ngOnInit(): void {
    this.presentsService.getRecipients().subscribe(data => this.recipients = data);
  }

  addSuggestion(recipient: Recipient) {
    let suggestion: Suggestion = {
      id: Math.max(...recipient.suggestions.map(r => r.id)) + 1,
      label: '',
      urls: [],
      status: Status.considered,
      recipient: recipient.id,
    }

    const dialogRef = this.dialog.open(SuggestionDialogComponent, {
      width: '250px',
      data: suggestion,
    });

    dialogRef.afterClosed().subscribe(async result => {
      // alert(result);
      recipient.suggestions.push(result);
      // this.presentsService.createSuggestion(result).subscribe(data => console.log(data));
      await lastValueFrom(this.presentsService.createSuggestion(result));
    });
  }

  addRecipient() {
    let recipient: Recipient = {
      id: Math.max(...this.recipients.map(r => r.id)) + 1,
      recipientname: '',
      suggestions: [],
    }

    const dialogRef = this.dialog.open(RecipientDialogComponent, {
      width: '250px',
      data: recipient,
    });

    dialogRef.afterClosed().subscribe(async result => {
      // alert(result);
      this.recipients.push(result);
      // this.presentsService.createRecipient(result).subscribe(data => console.log(data));
      await lastValueFrom(this.presentsService.createRecipient(result));
    });
  }

  async removeRecipient(recipient: Recipient) {
    this.recipients = this.recipients.filter(r => r.id !== recipient.id);
    // this.presentsService.deleteRecipient(recipient).subscribe(data => console.log(data));
    await lastValueFrom(this.presentsService.deleteRecipient(recipient));
  }

  async removeSuggestion(recipient: Recipient, suggestion: Suggestion) {
    recipient.suggestions = recipient.suggestions.filter(s => s.id !== suggestion.id);
    // this.presentsService.deleteSuggestion(suggestion).subscribe(data => console.log(data));
    await lastValueFrom(this.presentsService.deleteSuggestion(suggestion));
  }
}

@Component({
  selector: 'app-recipient-dialog',
  template: `
    <p>Who’s the present for?</p>
    <mat-form-field appearance="fill">
      <input matInput [(ngModel)]="data.recipientname">
    </mat-form-field>

    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">Cancel</button>
      <button mat-button [matDialogClose]="data" cdkFocusInitial>Ok</button>
    </div>
  `
})
export class RecipientDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<RecipientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Recipient,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-suggestion-dialog',
  template: `
    <p>What is the gift supposed to be?</p>
    <mat-form-field appearance="fill">
      <input matInput [(ngModel)]="data.label">
    </mat-form-field>

    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">Cancel</button>
      <button mat-button [matDialogClose]="data" cdkFocusInitial>Ok</button>
    </div>
  `
})
export class SuggestionDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<SuggestionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Suggestion,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
