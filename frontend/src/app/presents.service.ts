import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Recipient, Suggestion } from './model'

@Injectable({
  providedIn: 'root'
})
export class PresentsService {
  recipients: Recipient[] = []

  constructor(
    private http: HttpClient,
  ) { }

  getRecipients() {
    // return this.http.get<Recipient[]>('/assets/recipients.json');
    return this.http.get<Recipient[]>('http://localhost:8000/recipients/');
  }

  createRecipient(recipient: Recipient) {
    return this.http.post<Recipient>(
      'http://localhost:8000/recipients/',
      recipient);
  }

  editRecipient(recipient: Recipient) {
    return this.http.put<Recipient>(
      `http://localhost:8000/recipient/${recipient.id}`,
        recipient);
  }

  deleteRecipient(recipient: Recipient) {
    return this.http.delete<Recipient>(
      `http://localhost:8000/recipient/${recipient.id}`)
  }

  createSuggestion(suggestion: Suggestion) {
    return this.http.post<Suggestion>(
      'http://localhost:8000/suggestions/',
      suggestion);
  }

  editSuggestion(suggestion: Suggestion) {
    return this.http.put<Suggestion>(
      `http://localhost:8000/suggestion/${suggestion.id}`,
        suggestion);
  }

  deleteSuggestion(suggestion: Suggestion) {
    return this.http.delete<Suggestion>(
      `http://localhost:8000/suggestion/${suggestion.id}`)
  } 
}
