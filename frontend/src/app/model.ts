export enum Status {
	considered = "considered",
	ordered = "ordered",
	paid = "paid",
	delivered = "delivered",
}

export interface Suggestion {
	id: number,
	label: string,
	urls: string[],
	status: Status,
	recipient: number,
}

export interface Recipient {
	id: number,
	recipientname: string,
	suggestions: Suggestion[],
}

export interface User {
	username: string,
	email: string,
	first_name: string,
	last_name: string,
	password: string,
}
