import { Component, Input, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatSelectChange} from '@angular/material/select';

import {PresentsService} from '../presents.service';
import { Status, Suggestion } from '../model'
import {lastValueFrom} from 'rxjs';

@Component({
  selector: 'app-suggestion',
  templateUrl: './suggestion.component.html',
  styleUrls: ['./suggestion.component.scss']
})
export class SuggestionComponent implements OnInit {
  @Input() suggestion!: Suggestion;
  @Output() suggestionDelete = new EventEmitter<Suggestion>();
  status!: Status;
  statuskeys: string[] = [];

  constructor(
    private presentsService: PresentsService,
    private dialog: MatDialog,
  ) {
    this.statuskeys = Object.keys(Status);
  }

  ngOnInit(): void {
  }

  addUrl() {
    const dialogRef = this.dialog.open(AddUrlDialogComponent, {
      width: '250px',
      data: '',
    });

    dialogRef.afterClosed().subscribe(result => {
      // alert(result.label + ' ' + result.urls);
      this.suggestion.urls.push(result);
      this.changeSuggestion(this.suggestion);
    });
  }

  changeStatus(suggestion: Suggestion, $event: MatSelectChange) {
    // console.log(suggestion);
    // console.log($event.value);
    this.changeSuggestion(suggestion);
  }

  async changeSuggestion(suggestion: Suggestion) {
    // this.presentsService.editSuggestion(suggestion).subscribe(result => console.log(result));
    await lastValueFrom(this.presentsService.editSuggestion(suggestion));
  }

  removeSuggestion(suggestion: Suggestion) {
    this.suggestionDelete.emit(suggestion);
  }
}

@Component({
  selector: 'app-addurl-dialog',
  template: `
    <p>Add URL</p>
    <mat-form-field appearance="fill">
      <input matInput [(ngModel)]="data">
    </mat-form-field>

    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">Cancel</button>
      <button mat-button [matDialogClose]="data" cdkFocusInitial>Ok</button>
    </div>
  `
})
export class AddUrlDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<AddUrlDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
