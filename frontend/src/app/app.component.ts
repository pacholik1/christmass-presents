import { Component, Inject } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AuthServiceService} from './auth-service.service';
import {User} from './model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Presents';
  // public isAuthenticated = false;

  constructor(
    private dialog: MatDialog,
    private authservice: AuthServiceService,
  ) {
  }

  get isAuthenticated() {
    return this.authservice.isLogged();
  }

  public logout(): void {
    // this.isAuthenticated = false;
    // delete also token here
  }

  login(): void {
    let user: User = {
      username: '',
      email: '',
      first_name: '',
      last_name: '',
      password: '',
    }

    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '250px',
      data: user,
    });

    dialogRef.afterClosed().subscribe(async result => {
      this.authservice.login(result.username, result.password).subscribe(
        result => {
          console.log(result);
          // this.isAuthenticated = true;
        },
        error => {
          console.log(error);
        },
        () => {
          // oncomplete
        }
      );
    });
  }

  signup(): void {
    let user: User = {
      username: '',
      email: '',
      first_name: '',
      last_name: '',
      password: '',
    }

    const dialogRef = this.dialog.open(SignupDialogComponent, {
      width: '250px',
      data: user,
    });

    dialogRef.afterClosed().subscribe(async result => {
      this.authservice.signup(
        result.username,
        result.email,
        result.password,
      ).subscribe(
        result => {
          console.log(result);
          // this.isAuthenticated = true;
        },
        error => {
          console.log(error);
        },
        () => {
          // oncomplete
        }
      );
    });
  }
}

@Component({
  selector: 'app-login-dialog',
  template: `
    <form [formGroup]="loginform">
      <p>User Name</p>
      <mat-form-field appearance="fill">
        <input matInput formControlName="username" [(ngModel)]="data.username">
      </mat-form-field>
      <p>Password</p>
      <mat-form-field appearance="fill">
        <input matInput type="password" formControlName="password" [(ngModel)]="data.password">
      </mat-form-field>

      <div mat-dialog-actions>
        <button mat-button (click)="onNoClick()">Cancel</button>
        <button mat-button [disabled]="!loginform.valid" [matDialogClose]="data" cdkFocusInitial>Login</button>
      </div>
    </form>
  `
})
export class LoginDialogComponent {
  public loginform: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
  ) {
    this.loginform = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-signup-dialog',
  template: `
    <form [formGroup]="signupform">
      <p>User Name</p>
      <mat-form-field appearance="fill">
        <input matInput formControlName="username" [(ngModel)]="data.username">
        <mat-error *ngIf="f['username'].errors?.['required']">Username is required</mat-error>
      </mat-form-field>
      <p>Email</p>
      <mat-form-field appearance="fill">
        <input matInput formControlName="email" [(ngModel)]="data.email">
        <mat-error *ngIf="f['email'].errors?.['required']">Email is required</mat-error>
        <mat-error *ngIf="f['email'].errors?.['email']">Not an email address</mat-error>
      </mat-form-field>
      <p>Password</p>
      <mat-form-field appearance="fill">
        <input matInput type="password" formControlName="password" [(ngModel)]="data.password">
        <mat-error *ngIf="f['password'].errors?.['required']">Password is required</mat-error>
      </mat-form-field>
      <p>Password</p>
      <mat-form-field appearance="fill">
        <input matInput type="password" name="password_confirm" formControlName="password_confirm">
        <mat-error *ngIf="f['password_confirm'].errors?.['confirmedValidator']">Passwords don’t match</mat-error>
      </mat-form-field>

      <div mat-dialog-actions>
        <button mat-button (click)="onNoClick()">Cancel</button>
        <button mat-button [disabled]="!signupform.valid" [matDialogClose]="data" cdkFocusInitial>Signup</button>
      </div>
    </form>
  `
})
export class SignupDialogComponent {
  public signupform: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<SignupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private fb: FormBuilder,
  ) {
    this.signupform = fb.group({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      // first_name: new FormControl('', []),
      // last_name: new FormControl('', []),
      password: new FormControl('', [Validators.required]),
      password_confirm: new FormControl('', [Validators.required]),
    },
    {
      validators: this.confirmedValidator('password', 'password_confirm'),
    });
  }

  get f() {
    return this.signupform.controls;
  }

  confirmedValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors['confirmedValidator']) {
            return;
        }
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
